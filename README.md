# adb-fastboot刷机工具包

## 简介

本仓库提供了一个adb-fastboot刷机工具包，包含了常用的adb和fastboot命令，方便用户进行Android设备的刷机操作。无论是开发者还是普通用户，都可以通过这些命令轻松管理设备，进行重启、刷写分区等操作。

## 资源文件内容

### adb常用命令

- `adb devices`：列出当前连接的adb设备。
- `adb reboot`：重启设备。
- `adb reboot bootloader`：重启设备到fastboot模式。
- `adb reboot recovery`：重启设备到recovery模式。

### fastboot常用命令

- `fastboot devices`：列出当前连接的fastboot设备。
- `fastboot reboot`：重启设备。
- `fastboot reboot-bootloader`：重启设备到fastboot模式。
- `fastboot reboot-recovery`：重启设备到recovery模式。
- `fastboot oem device-info`：查看设备的解锁状态。
- `fastboot oem reboot-<模式名称>`：重启设备到相应的模式。
- `fastboot getvar product`：查询设备的代号名称。
- `fastboot flash <分区名称> <镜像文件名>`：刷写对应分区的镜像文件。

## 使用说明

1. **下载工具包**：首先，下载本仓库提供的adb-fastboot刷机工具包。
2. **解压文件**：将下载的压缩包解压到本地目录。
3. **打开命令行工具**：在解压后的目录中打开命令行工具（如Windows的CMD或PowerShell，Linux或Mac的终端）。
4. **执行命令**：根据需要执行上述提供的adb或fastboot命令。

## 注意事项

- 在进行刷机操作前，请确保备份重要数据，以防数据丢失。
- 请确保设备已解锁Bootloader，否则部分fastboot命令可能无法执行。
- 刷机操作具有一定风险，请谨慎操作，避免对设备造成不可逆的损坏。

## 贡献

如果您在使用过程中发现任何问题或有改进建议，欢迎提交Issue或Pull Request。

## 许可证

本项目采用MIT许可证，详情请参阅LICENSE文件。